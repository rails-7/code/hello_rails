Rails.application.routes.draw do
  get 'welcome/hello'
  get 'welcome/wish'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
